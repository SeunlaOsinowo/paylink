  @for( $i=1; $i<=20; $i++ )
  <p>The Value of i is {{ $i }} i.e:
                                            @if($i % 3 == 0)
                                                Fizz
                                            @elseif($i % 5 == 0)
                                                Buzz
                                            @elseif($i % 15 == 0)
                                                FizzBuzz
                                            @else
                                                {{ $i }}
                                            @endif
                                        @endfor
                                        </p>