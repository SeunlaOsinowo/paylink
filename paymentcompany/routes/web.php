<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('question1', function () {
    return view('question1');
});





Route::get('question2', function () {
function Fibonacci($n){ 

    $Number1 = 0; $Number2 = 1; $count = 0; 
    while ($count < $n){ 
        echo ' '.$Number1; 
        $Number3 = $Number2 + $Number1; 
        $Number1 = $Number2; 
        $Number2 = $Number3; 
        $count = $count + 1; 

    } 
} 

 

$n = 10;
$a = 5; 
$j = 1;
Fibonacci($n);

echo "<br>"; 
Fibonacci($a); 
echo "<br>";
Fibonacci($j); 


    return view('question2',compact('n'));
});





Route::get('question3', function () {


    class PayLink {
    private $fieldA;
    private $fieldB;

    public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
    }

    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }
}

$PayLink = new PayLink();

$PayLink->fieldA = "This is A";
$PayLink->fieldB = "This is B";

echo $PayLink->fieldA;
echo "<br>";
echo $PayLink->fieldB;



    return view('question3');
});


Route::get('fizzbuzz', function () {
    $data = "seunla";
    return view('fizzbuzz',compact('data'));
});
